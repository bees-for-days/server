package sensors

import (
	"errors"

	"gorm.io/gorm"
)

type SensorEntryRepository struct {
	db *gorm.DB
}

func NewSensorEntryRepository(dbConn *gorm.DB) *SensorEntryRepository {
	return &SensorEntryRepository{dbConn}
}

// ListForSensor returns entries, cursor is the last id fetched in a previous list function
func (r *SensorEntryRepository) ListForSensor(sensorID uint, cursorID uint, limit int) ([]SensorEntry, error) {
	list := []SensorEntry{}

	return list, r.db.Model(&SensorEntry{}).
		Where("id > ? AND sensor_id = ?", cursorID, sensorID).
		Limit(limit).
		Find(&list).
		Error
}

func (r *SensorEntryRepository) Create(entry *SensorEntry) error {
	if entry.ID != 0 {
		return errors.New("not allowed to update sensor entry")
	}

	return r.db.Create(entry).Error
}
