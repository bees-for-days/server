package sensors

import (
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/render"

	"gitlab.com/bees-for-days/server/encrypt"
	"gitlab.com/bees-for-days/server/hives"
	"gorm.io/gorm"
)

type Sensor struct {
	gorm.Model
	HiveID     uint
	Hive       hives.Hive `gorm:"foreignKey:HiveID;constraint:OnDelete:SET NULL,OnUpdate:CASCADE" json:"-"`
	Type       string
	HardwareID string `json:"hardwareID"`
	// SecretKey is only allowed to be created, never updated
	SecretKey []byte `gorm:"<-:create,type:bytea" json:"-"`
	// SecretKeyRaw is meant for the initial return of Sensor, it should almost never be filled after creation
	SecretKeyRaw string `gorm:"-" json:",omitempty"`
	SetupMode    bool   // SetupMode lets us dictate to the hardware that it should turn on it's onboard wifi
	CreatedBy    string
	ModifiedBy   string
}

// Decrypt returns the decrypted SecretKey
func (s *Sensor) Decrypt() []byte {
	return encrypt.Decrypt(s.SecretKey)
}

type SensorRequest struct {
	*Sensor
}

func (s *SensorRequest) Bind(r *http.Request) error {
	if s.Sensor == nil {
		return errors.New("missing required sensor fields")
	}
	return nil
}

type SensorResponse struct {
	*Sensor
}

func NewSensorResponse(sensor *Sensor) *SensorResponse {
	return &SensorResponse{sensor}
}

func (sr *SensorResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func NewSensorListResponse(sensors []Sensor) []render.Renderer {
	list := []render.Renderer{}
	for i := range sensors {
		list = append(list, NewSensorResponse(&sensors[i]))
	}
	return list
}

type SensorEntry struct {
	gorm.Model
	SensorID     uint
	Sensor       Sensor `gorm:"foreignKey:SensorID;constraint:OnDelete:SET NULL,OnUpdate:CASCADE" json:"-"`
	HardwareID   string `json:"hardwareID"`
	RecordedTime time.Time
	Signature    string
	Data         map[string]interface{} `gorm:"type:jsonb"`
}

type SensorEntryRequest struct {
	*SensorEntry
}

func (s *SensorEntryRequest) Bind(r *http.Request) error {
	if s.SensorEntry == nil {
		return errors.New("missing required fields")
	}

	return nil
}

type SensorEntryResponse struct {
	*SensorEntry
}

func NewSensorEntryResponse(entry *SensorEntry) *SensorEntryResponse {
	return &SensorEntryResponse{entry}
}

func (er *SensorEntryResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func NewSensorEntryListResponse(entries []SensorEntry) []render.Renderer {
	list := []render.Renderer{}
	for i := range entries {
		list = append(list, NewSensorEntryResponse(&entries[i]))
	}
	return list
}
