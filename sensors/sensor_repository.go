package sensors

import (
	"errors"

	"gitlab.com/bees-for-days/server/encrypt"

	"gitlab.com/bees-for-days/server/keepers"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type SensorRepository struct {
	db *gorm.DB
}

func NewSensorRepository(dbConn *gorm.DB) *SensorRepository {
	return &SensorRepository{dbConn}
}

func (r *SensorRepository) Delete(keeper keepers.Keeper, sensor Sensor, options *DeleteOptions) error {
	// check zero value of unit - gorm will drop whole table if you run  delete with a zero value
	if sensor.ID == 0 {
		return errors.New("sensor must have id")
	}

	if options != nil && options.Force {
		return r.db.Unscoped().Delete(&sensor).Error
	}

	sensor.ModifiedBy = keeper.ID
	return r.db.Delete(&sensor).Error
}

func (r *SensorRepository) FindByIDAndHive(id uint, hiveID uint) (Sensor, error) {
	sensor := Sensor{}

	return sensor, r.db.Where("id = ? and hive_id = ? AND deleted_at is NULL", id, hiveID).
		Preload("Hive").
		First(&sensor).Error
}

func (r *SensorRepository) FindByHardwareID(id string) (Sensor, error) {
	sensor := Sensor{}

	return sensor, r.db.Where("hardware_id = ?", id).
		Preload("Hive").
		First(&sensor).Error
}

func (r *SensorRepository) ListForHive(hiveID uint) ([]Sensor, error) {
	list := []Sensor{}

	return list, r.db.Model(&Sensor{}).
		Where("hive_id = ?", hiveID).
		Preload("Hive").
		Find(&list).
		Error
}

func (r *SensorRepository) Save(keeper keepers.Keeper, sensor *Sensor) error {
	sensor.ModifiedBy = keeper.ID

	// also create and encrypt the secret key used for HMAC validation
	if sensor.ID == 0 {
		sensor.CreatedBy = keeper.ID
		sensor.SecretKeyRaw = encrypt.TokenGenerator()
		sensor.SecretKey = encrypt.Encrypt([]byte(sensor.SecretKeyRaw))
	}

	return r.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(sensor).Error
}

type DeleteOptions struct {
	Force bool
}
