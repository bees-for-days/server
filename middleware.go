package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/bees-for-days/server/hives"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/bees-for-days/server/apiaries"
	"gitlab.com/bees-for-days/server/keepers"
)

// getKeeper should be run AFTER the authJWT middleware. This insures that the
// keeper id we fetch and build the struct for is a valid token and we are truly
// acting on behalf of the user
func getKeeper(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		token, ok := ctx.Value("jwt").(*jwt.Token)
		if !ok {
			http.Error(w, "jwt not present in context or unparsable", 500)
			return
		}

		claims, ok := token.Claims.(*jwt.StandardClaims)
		if !ok {
			http.Error(w, "error processing standard claims", 500)
			return
		}

		newCtx := context.WithValue(ctx, "keeper", keepers.Keeper{ID: claims.Subject})
		next.ServeHTTP(w, r.WithContext(newCtx))
		return
	})
}

func authJWT(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Get token from authorization header.
		bearer := r.Header.Get("Authorization")
		if len(bearer) > 7 && strings.ToUpper(bearer[0:6]) == "BEARER" {
			token := bearer[7:]

			validatedToken, err := jwt.ParseWithClaims(token, &jwt.StandardClaims{}, func(t *jwt.Token) (interface{}, error) {
				if _, ok := t.Method.(*jwt.SigningMethodRSA); !ok {
					return nil, fmt.Errorf(
						"expected token algorithm '%v' but got '%v'",
						jwt.SigningMethodRS256.Name,
						t.Header)
				}

				cert, err := getPemCert(t)
				if err != nil {
					return nil, err
				}

				// Since the token is RSA (which we validated at the start of this function), the return type of this function actually has to be rsa.PublicKey!
				pubKey, err := jwt.ParseRSAPublicKeyFromPEM([]byte(cert))
				if err != nil {
					return nil, fmt.Errorf("an error occurred parsing the public key base64 for key ID '%v'; this is a code bug", t.Header["kid"])
				}

				return pubKey, nil
			})

			if err != nil || !validatedToken.Valid {
				http.Error(w, "token invalid", 401)
				return
			}

			// we pass the entire validated token to context, our next middleware will handle it
			// and we do not need any of the claims but the standard subject claim
			ctx := context.WithValue(r.Context(), "jwt", validatedToken)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		http.Error(w, "bearer token not present", 401)
		return
	})
}

type Jwks struct {
	Keys []JSONWebKeys `json:"keys"`
}

type JSONWebKeys struct {
	Kty string   `json:"kty"`
	Kid string   `json:"kid"`
	Use string   `json:"use"`
	N   string   `json:"n"`
	E   string   `json:"e"`
	X5c []string `json:"x5c"`
}

func getPemCert(token *jwt.Token) (string, error) {
	cert := ""
	resp, err := http.Get(fmt.Sprintf("%s/.well-known/jwks.json", os.Getenv("OAUTH_DOMAIN")))

	if err != nil {
		return cert, err
	}
	defer resp.Body.Close()

	var jwks = Jwks{}
	err = json.NewDecoder(resp.Body).Decode(&jwks)

	if err != nil {
		return cert, err
	}

	for k := range jwks.Keys {
		if token.Header["kid"] == jwks.Keys[k].Kid {
			cert = "-----BEGIN CERTIFICATE-----\n" + jwks.Keys[k].X5c[0] + "\n-----END CERTIFICATE-----"
		}
	}

	if cert == "" {
		err := errors.New("unable to find appropriate key")
		return cert, err
	}

	return cert, nil
}

func ApiaryCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		repo := apiaries.NewApiaryRepository(dbConn)
		if repo == nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
		if !ok {
			render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
			return
		}

		apiaryID := chi.URLParam(r, "apiaryID")
		apiary, err := repo.FindByIDForKeeper(toUint(apiaryID), keeper)
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		ctx := context.WithValue(r.Context(), "apiary", apiary)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// HiveCtx relies on the ApiaryCtx function to have been called earlier in the handler stack
// doing this allows us to ensure we're not fetching a hive that doesn't belong to the apiary
// in the handler stack - we don't need to check the keeper's status as the previous middleware
// will have taken care of that
func HiveCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		repo := apiaries.NewApiaryRepository(dbConn)
		if repo == nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		apiary, ok := r.Context().Value("apiary").(apiaries.Apiary)
		if !ok {
			render.Render(w, r, ErrRender(errors.New("unable to pull apiary from context")))
			return
		}

		hive, err := hiveRepo(dbConn).FindByIDAndApiary(toUint(chi.URLParam(r, "hiveID")), apiary.ID)
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		ctx := context.WithValue(r.Context(), "hive", hive)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// SensorCtx relies on HiveCtx to ensure that the caller actually has the permission
// to access the sensor designed in the url. We don't need to check keeper's status
// because that will have been done earlier in the stack
func SensorCtx(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		hive, ok := r.Context().Value("hive").(hives.Hive)
		if !ok {
			render.Render(w, r, ErrRender(errors.New("unable to pull hive from context")))
			return
		}

		sensor, err := sensorRepo(dbConn).FindByIDAndHive(toUint(chi.URLParam(r, "sensorID")), hive.ID)
		if err != nil {
			http.Error(w, http.StatusText(404), 404)
			return
		}

		ctx := context.WithValue(r.Context(), "sensor", sensor)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
