package apiaries

import (
	"errors"

	"gitlab.com/bees-for-days/server/keepers"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type ApiaryRepository struct {
	db *gorm.DB
}

func NewApiaryRepository(db *gorm.DB) *ApiaryRepository {
	return &ApiaryRepository{db}
}

func (r *ApiaryRepository) FindByID(id uint) (Apiary, error) {
	apiary := Apiary{}

	return apiary, r.db.Where("id = ?", id).First(&apiary).Error
}

func (r *ApiaryRepository) FindByIDForKeeper(id uint, keeper keepers.Keeper) (Apiary, error) {
	apiary := Apiary{}

	return apiary, r.db.Model(&KeeperApiary{}).
		Select("*").
		Joins("LEFT JOIN apiaries on apiaries.id = keeper_apiaries.apiary_id").
		Where("keeper_apiaries.keeper_id = ? AND apiaries.id = ? AND apiaries.deleted_at IS NULL", keeper.ID, id).
		First(&apiary).
		Error
}

func (r *ApiaryRepository) Delete(keeper keepers.Keeper, apiary Apiary, options *DeleteOptions) error {
	// check zero value of unit - gorm will drop whole table if you run  delete with a zero value
	if apiary.ID == 0 {
		return errors.New("apiary must have id")
	}

	if options != nil && options.Force {
		return r.db.Unscoped().Delete(&apiary).Error
	}

	apiary.ModifiedBy = keeper.ID
	return r.db.Delete(&apiary).Error
}

// Save is an upsert function, on conflict it will update all changed fields except primary key (id)
func (r *ApiaryRepository) Save(keeper keepers.Keeper, toSave ...*Apiary) error {
	for _, apiary := range toSave {
		apiary.ModifiedBy = keeper.ID

		if apiary.ID == 0 {
			apiary.CreatedBy = keeper.ID
		}
	}

	err := r.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(toSave).Error

	if err != nil {
		return err
	}

	for _, apiary := range toSave {
		if err := r.db.Clauses(clause.OnConflict{
			UpdateAll: true,
			Columns:   []clause.Column{{Name: "apiary_id"}},
		}).Create(&KeeperApiary{
			KeeperID: keeper.ID,
			Apiary:   *apiary,
		}).Error; err != nil {
			return err
		}
	}

	return nil
}

func (r *ApiaryRepository) ListByKeeper(keeper keepers.Keeper) ([]Apiary, error) {
	list := []Apiary{}

	return list, r.db.Model(&KeeperApiary{}).
		Select("*").
		Joins("LEFT JOIN apiaries ON apiaries.id = keeper_apiaries.apiary_id ").
		Where("keeper_apiaries.keeper_id = ? AND apiaries.deleted_at IS NULL", keeper.ID).
		Find(&list).
		Error
}

type DeleteOptions struct {
	Force bool
}
