package apiaries

import (
	"errors"
	"net/http"

	"github.com/go-chi/render"

	"gitlab.com/bees-for-days/server/hives"
	"gorm.io/gorm"
)

type Apiary struct {
	gorm.Model
	Name        string
	Description string
	Latitude    string
	Longitude   string
	Altitude    string
	Hives       []hives.Hive `gorm:"constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	CreatedBy   string
	ModifiedBy  string
}

// KeeperApiary is how we'll keep track of which apiaries a user owns. The KeeperID will be
// the Auth0 user id, which means no foreign key magic for us. While the ApiaryID is unique
// (meaning that there will only ever be one keeper per apiary) doing the table this way lets
// us change in the future
type KeeperApiary struct {
	ApiaryID uint   `gorm:"unique"`
	Apiary   Apiary `gorm:"foreignKey:ApiaryID"`
	KeeperID string `gorm:"index"`
}

// The request and response bodies below feel like a lot of work for something so
// simple, but doing it this way paves the way for more complicated options further
// down the road

type ApiaryRequest struct {
	*Apiary
}

func (a *ApiaryRequest) Bind(r *http.Request) error {
	if a.Apiary == nil {
		return errors.New("missing required Apiary fields")
	}
	return nil
}

type ApiaryResponse struct {
	*Apiary
}

func NewApiaryResponse(apiary *Apiary) *ApiaryResponse {
	return &ApiaryResponse{apiary}
}

func (ar *ApiaryResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func NewApiaryListResponse(apiaries []Apiary) []render.Renderer {
	list := []render.Renderer{}
	for i := range apiaries {
		list = append(list, NewApiaryResponse(&apiaries[i]))
	}
	return list
}
