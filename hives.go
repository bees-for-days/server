package main

import (
	"errors"
	"net/http"

	"gitlab.com/bees-for-days/server/apiaries"

	"github.com/go-chi/render"
	"gitlab.com/bees-for-days/server/keepers"

	"github.com/go-chi/chi/v5"

	"gitlab.com/bees-for-days/server/hives"
)

var hiveRepo = hives.NewHiveRepository

// hive router must be used after ApiaryCtx has been added to the handler stack
func hiveRouter(middlewares ...func(handler http.Handler) http.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middlewares...)

	r.Post("/", createHive)
	r.Get("/", listHives)

	r.Route("/{hiveID}", func(r chi.Router) {
		r.Use(HiveCtx)
		r.Get("/", getHive)
		r.Put("/", updateHive)
		r.Delete("/", deleteHive)

		r.Mount("/sensors", sensorRouter())
	})

	return r
}

func createHive(w http.ResponseWriter, r *http.Request) {
	data := &hives.HiveRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	apiary, ok := r.Context().Value("apiary").(apiaries.Apiary)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull apiary from context")))
		return
	}

	data.Hive.ApiaryID = apiary.ID

	err := hiveRepo(dbConn).Save(keeper, data.Hive)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusCreated)
	render.Render(w, r, hives.NewHiveResponse(data.Hive))
}

func updateHive(w http.ResponseWriter, r *http.Request) {
	oldHive := r.Context().Value("hive").(hives.Hive)

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	data := &hives.HiveRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	oldHive.Name = data.Name
	oldHive.Description = data.Description

	if err := hiveRepo(dbConn).Save(keeper, &oldHive); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.Render(w, r, hives.NewHiveResponse(&oldHive)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func getHive(w http.ResponseWriter, r *http.Request) {
	hive := r.Context().Value("hive").(hives.Hive)

	if err := render.Render(w, r, hives.NewHiveResponse(&hive)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func listHives(w http.ResponseWriter, r *http.Request) {
	apiary, ok := r.Context().Value("apiary").(apiaries.Apiary)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull apiary from context")))
		return
	}

	results, err := hiveRepo(dbConn).ListForApiary(apiary.ID)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.RenderList(w, r, hives.NewHiveListResponse(results)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func deleteHive(w http.ResponseWriter, r *http.Request) {
	hive := r.Context().Value("hive").(hives.Hive)

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	if err := hiveRepo(dbConn).Delete(keeper, hive, nil); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusOK)
}
