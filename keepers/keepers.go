package keepers

// Keeper is not held inside the database. This is a data structure used in conjunction with
// the AuthO identity service. This is to hold information we get from the token from the AuthO
// system
type Keeper struct {
	ID string
}
