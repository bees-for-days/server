package main

import (
	"errors"
	"net/http"

	"gitlab.com/bees-for-days/server/keepers"

	"github.com/go-chi/render"
	"gitlab.com/bees-for-days/server/apiaries"

	"github.com/go-chi/chi/v5"
)

var apiaryRepo = apiaries.NewApiaryRepository

func apiaryRouter(middlewares ...func(handler http.Handler) http.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middlewares...)

	r.Post("/", createApiary)
	r.Get("/", listApiaries)

	r.Route("/{apiaryID}", func(r chi.Router) {
		r.Use(ApiaryCtx)
		r.Get("/", getApiary)
		r.Put("/", updateApiary)
		r.Delete("/", deleteApiary)

		r.Mount("/hives", hiveRouter())
	})

	return r
}

func createApiary(w http.ResponseWriter, r *http.Request) {
	data := &apiaries.ApiaryRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	err := apiaryRepo(dbConn).Save(keeper, data.Apiary)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusCreated)
	render.Render(w, r, apiaries.NewApiaryResponse(data.Apiary))
}

func getApiary(w http.ResponseWriter, r *http.Request) {
	apiary := r.Context().Value("apiary").(apiaries.Apiary)

	if err := render.Render(w, r, apiaries.NewApiaryResponse(&apiary)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func updateApiary(w http.ResponseWriter, r *http.Request) {
	oldApiary := r.Context().Value("apiary").(apiaries.Apiary)

	data := &apiaries.ApiaryRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	// update only the fields we choose
	oldApiary.Name = data.Name
	oldApiary.Description = data.Description
	oldApiary.Altitude = data.Altitude
	oldApiary.Longitude = data.Longitude
	oldApiary.Latitude = data.Latitude

	err := apiaryRepo(dbConn).Save(keeper, &oldApiary)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.Render(w, r, apiaries.NewApiaryResponse(&oldApiary)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func listApiaries(w http.ResponseWriter, r *http.Request) {
	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	results, err := apiaryRepo(dbConn).ListByKeeper(keeper)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.RenderList(w, r, apiaries.NewApiaryListResponse(results)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

// the apiary context will handle 404's for articles not existing
func deleteApiary(w http.ResponseWriter, r *http.Request) {
	apiary := r.Context().Value("apiary").(apiaries.Apiary)

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	if err := apiaryRepo(dbConn).Delete(keeper, apiary, nil); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusOK)
}
