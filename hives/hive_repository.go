package hives

import (
	"errors"

	"gitlab.com/bees-for-days/server/keepers"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type HiveRepository struct {
	db *gorm.DB
}

func NewHiveRepository(dbConn *gorm.DB) *HiveRepository {
	return &HiveRepository{dbConn}
}

func (r *HiveRepository) FindByID(id uint) (Hive, error) {
	hive := Hive{}

	return hive, r.db.Where("id = ? AND deleted_at IS NULL", id).First(&hive).Error
}

func (r *HiveRepository) FindByIDAndApiary(id uint, apiaryID uint) (Hive, error) {
	hive := Hive{}

	return hive, r.db.Where("id = ? AND apiary_id = ? AND deleted_at IS NULL", id, apiaryID).
		First(&hive).Error
}

func (r *HiveRepository) ListForApiary(apiaryID uint) ([]Hive, error) {
	list := []Hive{}

	return list, r.db.Model(&Hive{}).
		Where("apiary_id = ?", apiaryID).
		Find(&list).
		Error
}

func (r *HiveRepository) Delete(keeper keepers.Keeper, hive Hive, options *DeleteOptions) error {
	// check zero value of unit - gorm will drop whole table if you run  delete with a zero value
	if hive.ID == 0 {
		return errors.New("hive must have id")
	}

	if options != nil && options.Force {
		// Unscoped allows us to bypass gorm's soft delete function
		return r.db.Unscoped().Delete(&hive).Error
	}

	hive.ModifiedBy = keeper.ID
	return r.db.Delete(&hive).Error
}

// Save is an upsert function, on conflict it will update all changed fields except primary key (id)
func (r *HiveRepository) Save(keeper keepers.Keeper, toSave ...*Hive) error {
	for _, hive := range toSave {
		hive.ModifiedBy = keeper.ID

		if hive.ID == 0 {
			hive.CreatedBy = keeper.ID
		}
	}

	return r.db.Clauses(clause.OnConflict{
		UpdateAll: true,
	}).Create(toSave).Error
}

type DeleteOptions struct {
	Force bool
}
