package hives

import (
	"errors"
	"net/http"

	"github.com/go-chi/render"

	"gorm.io/gorm"
)

type Hive struct {
	gorm.Model
	ApiaryID    uint
	Name        string
	Description string
	CreatedBy   string
	ModifiedBy  string
}

// The request and response bodies below feel like a lot of work for something so
// simple, but doing it this way paves the way for more complicated options further
// down the road

type HiveRequest struct {
	*Hive
}

func (h *HiveRequest) Bind(r *http.Request) error {
	if h.Hive == nil {
		return errors.New("missing required Hive fields")
	}
	return nil
}

type HiveResponse struct {
	*Hive
}

func NewHiveResponse(hive *Hive) *HiveResponse {
	return &HiveResponse{hive}
}

func (hr *HiveResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func NewHiveListResponse(hives []Hive) []render.Renderer {
	list := []render.Renderer{}
	for i := range hives {
		list = append(list, NewHiveResponse(&hives[i]))
	}
	return list
}
