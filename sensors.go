package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/bees-for-days/server/hives"
	"gitlab.com/bees-for-days/server/keepers"
	"gitlab.com/bees-for-days/server/sensors"
)

var sensorRepo = sensors.NewSensorRepository
var sensorEntryRepo = sensors.NewSensorEntryRepository

// this is meant to be mounted in the hive router, meaning all the routes
// should have access to the hive and keeper context
func sensorRouter(middlewares ...func(handler http.Handler) http.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middlewares...)

	r.Post("/", attachSensor)
	r.Get("/", listSensors)

	r.Route("/{sensorID}", func(r chi.Router) {
		r.Use(SensorCtx)
		r.Get("/", getAttachedSensor)
		r.Put("/", setUpdateSensor)
		r.Delete("/", deleteSensor)

		r.Route("/entries", func(r chi.Router) {
			r.Get("/", listSensorEntries)
		})
	})
	return r
}

// this router is a standalone router and has a single endpoint, that of the
// sensor report which is secured not by the authJWT middleware, but by hmac
// signatures on the actual entry request body itself
func sensorReportRouter(middlewares ...func(handler http.Handler) http.Handler) http.Handler {
	r := chi.NewRouter()
	r.Use(middlewares...)

	r.Post("/", recordSensorReport)

	return r
}

func getAttachedSensor(w http.ResponseWriter, r *http.Request) {
	sensor := r.Context().Value("sensor").(sensors.Sensor)

	if err := render.Render(w, r, sensors.NewSensorResponse(&sensor)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func listSensors(w http.ResponseWriter, r *http.Request) {
	hive, ok := r.Context().Value("hive").(hives.Hive)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull hive from context")))
		return
	}

	results, err := sensorRepo(dbConn).ListForHive(hive.ID)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.RenderList(w, r, sensors.NewSensorListResponse(results)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func deleteSensor(w http.ResponseWriter, r *http.Request) {
	sensor := r.Context().Value("sensor").(sensors.Sensor)

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	if err := sensorRepo(dbConn).Delete(keeper, sensor, nil); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusOK)
}

func setUpdateSensor(w http.ResponseWriter, r *http.Request) {
	sensor := r.Context().Value("sensor").(sensors.Sensor)

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	sensor.SetupMode = true

	if err := sensorRepo(dbConn).Save(keeper, &sensor); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusOK)
}

func attachSensor(w http.ResponseWriter, r *http.Request) {
	data := &sensors.SensorRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}

	keeper, ok := r.Context().Value("keeper").(keepers.Keeper)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	hive, ok := r.Context().Value("hive").(hives.Hive)
	if !ok {
		render.Render(w, r, ErrRender(errors.New("unable to pull keeper from context")))
		return
	}

	data.HiveID = hive.ID

	err := sensorRepo(dbConn).Save(keeper, data.Sensor)

	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	render.Status(r, http.StatusCreated)
	render.Render(w, r, sensors.NewSensorResponse(data.Sensor))
}

func listSensorEntries(w http.ResponseWriter, r *http.Request) {
	limit := 100
	sensor := r.Context().Value("sensor").(sensors.Sensor)

	cursor := toUint(r.URL.Query().Get("cursor"))
	rawLimit := r.URL.Query().Get("limit")

	if rawLimit != "" {
		result, err := strconv.Atoi(rawLimit)
		if err == nil {
			limit = result
		}
	}

	results, err := sensorEntryRepo(dbConn).ListForSensor(sensor.ID, cursor, limit)
	if err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}

	if err := render.RenderList(w, r, sensors.NewSensorEntryListResponse(results)); err != nil {
		render.Render(w, r, ErrRender(err))
		return
	}
}

func recordSensorReport(w http.ResponseWriter, r *http.Request) {
	// first decode and bind the entry - this is all that we need from the request
	data := &sensors.SensorEntryRequest{}
	if err := render.Bind(r, data); err != nil {
		render.Render(w, r, ErrInvalidRequest(err))
		return
	}
	// we're going to run all this inside a go func so that we can return
	// immediately

	go func(req *sensors.SensorEntryRequest) {
		sensor, err := sensorRepo(dbConn).FindByHardwareID(req.HardwareID)
		if err != nil {
			fmt.Println("cannot find sensor")
			return
		}

		// decrypt the secret so that we can verify the hmac signature
		// the signature comes from combining recorded time and hardwareID as a string
		mac := hmac.New(sha256.New, sensor.Decrypt())
		mac.Write([]byte(fmt.Sprintf("%s%s", req.HardwareID, req.RecordedTime.String())))
		expectedMAC := mac.Sum(nil)

		sig, _ := hex.DecodeString(req.Signature)

		if !hmac.Equal(sig, expectedMAC) {
			fmt.Println("hmac signature matching failed")
			return
		}

		req.SensorEntry.SensorID = sensor.ID

		// now that we've verified everything, write the record
		err = sensorEntryRepo(dbConn).Create(req.SensorEntry)
		if err != nil {
			fmt.Println(err)
		}
	}(data)

	render.Status(r, http.StatusOK)
}
