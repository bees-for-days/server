module gitlab.com/bees-for-days/server

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi/v5 v5.0.3
	github.com/go-chi/httprate v0.5.1
	github.com/go-chi/render v1.0.1
	github.com/joho/godotenv v1.3.0
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.13
)
