package main

import (
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/bees-for-days/server/apiaries"
	"gitlab.com/bees-for-days/server/hives"
	"gitlab.com/bees-for-days/server/sensors"
	"gorm.io/driver/postgres"

	"gorm.io/gorm"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httprate"
	"github.com/joho/godotenv"
)

var dbConn *gorm.DB

func main() {
	// we don't need to check for an error as production won't be using a .env file
	godotenv.Load()

	connection, err := gorm.Open(postgres.Open(os.Getenv("DB_CONNECTION_STRING")), &gorm.Config{})
	if err != nil {
		log.Fatal("unable to open database connection")
		return
	}

	dbConn = connection

	if os.Getenv("MIGRATE") == "true" {
		dbConn.AutoMigrate(
			&apiaries.Apiary{},
			&apiaries.KeeperApiary{},
			&hives.Hive{},
			&sensors.Sensor{},
			&sensors.SensorEntry{},
		)
	}

	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))

	// we're going to rate limit as this is a development server running in the cloud
	r.Use(httprate.Limit(100, 1*time.Minute,
		httprate.WithKeyFuncs(httprate.KeyByIP, httprate.KeyByEndpoint)))

	r.Get("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(200)
	})

	r.Mount("/apiaries", apiaryRouter(authJWT, getKeeper))
	// no auth middleware for the report route, it has its own auth system
	// for insuring the report is coming from a valid, registered device
	r.Mount("/reports", sensorReportRouter())

	http.ListenAndServe(":8090", r)
}

func toUint(in string) uint {
	out, _ := strconv.ParseUint(in, 10, 32)

	return uint(out)
}
